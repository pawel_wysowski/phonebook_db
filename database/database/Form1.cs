﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace database
{
    public partial class Form1 : Form
    {
        string connection = "Server=sql11.freemysqlhosting.net;Database=sql11214009;Uid=sql11214009;Pwd=ZQtxSpU3K6;";
        MySqlConnection connect;
        MySqlCommand cmd;
        int id;
        
        public Form1()
        {
            InitializeComponent();
            id = 1;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            connect = new MySqlConnection(connection);
            connect.Open();
            LoadData();
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(connect.State == ConnectionState.Open)
            {
                connect.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                cmd = connect.CreateCommand();
                cmd.CommandText = "INSERT INTO PhoneBook VALUES(@ID,@Name,@Number)";
                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Parameters.AddWithValue("@Name", textBox1.Text);
                cmd.Parameters.AddWithValue("@Number", textBox2.Text);
                id++;
                cmd.ExecuteNonQuery();
                LoadData();

            }
            catch(Exception)
            {
                throw;
            }
        }

        void LoadData()
        {
            cmd = connect.CreateCommand();
            cmd.CommandText = "SELECT * FROM PhoneBook";
            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0].DefaultView;

        }
    }
}
